import React from 'react';
import {reduxExtend} from '../lib';

export class Example extends React.Component{
    constructor(props){
        super(props);

        this.handleAddUser = this.handleAddUser.bind(this);
        this.handleGetUser = this.handleGetUser.bind(this);
        this.handleDeleteUser = this.handleDeleteUser.bind(this);
    }

    handleAddUser(){
        const user = {id:1,name:"Example",email:"example@example.com"};
        this.Actions.saveUser(user);
    }

    handleGetUser(){
        this.Actions.getUser(1);
    }

    handleDeleteUser(){
        this.Services.UserService.deleteUser(1); // using services
        this.Actions.deleteUser(1);
    }

    render(){
        return (
            <div>
                <button onClick={this.handleAddUser}>Add user</button>
                <button onClick={this.handleGetUser}>Get user</button>
                <button onClick={this.handleDeleteUser}>Delete user</button>
                <div>{this.props.user && JSON.stringify(this.props.user)}</div>
            </div>
        );
    }
}

export default reduxExtend(Example);