'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _throttleDebounce = require('throttle-debounce');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DEFAULT_HTML = '\n<!DOCTYPE html>\n<html>\n<head></head>\n    <body>\n            <script>\n                function initMap(e) {\n                    window.status = JSON.stringify(e, ["type"]);\n                }\n                function loadScript(url, callback) {\n\n                    var head = document.getElementsByTagName(\'head\')[0];\n                    var script = document.createElement(\'script\');\n\n                    script.type = \'text/javascript\';\n                    script.onreadystatechange = callback;\n                    script.onload = callback;\n                    script.onerror = callback;\n                    script.src = url;\n\n                    head.appendChild(script);\n                }\n\n                loadScript("%script%", initMap);\n\n                </script>\n    </body>\n    </html>';

var DEFAULT_MAP_OPTIONS = {
    resources: 'usable',
    runScripts: "dangerously",
    url: "http://localhost:8080",
    referrer: "http://localhost:8080",
    contentType: "text/html",
    userAgent: "Mellblomenator/9000"
};

var TestHelper = function () {
    function TestHelper() {
        (0, _classCallCheck3.default)(this, TestHelper);
    }

    (0, _createClass3.default)(TestHelper, null, [{
        key: 'getComponent',
        value: function getComponent(component, type) {
            if (!component) return null;

            if (component._component) component = component._component;

            if (component._reactInternalFiber || component._reactInternalInstance) component = component._reactInternalFiber || component._reactInternalInstance;

            if (!component) return null;

            if (!component._renderedComponent && !component.child) return null;

            var rendered = component._renderedComponent || component.child;
            if (rendered && (!rendered._instance || rendered._instance.constructor.name !== type.name) && (!rendered.stateNode || rendered.stateNode.constructor.name !== type.name)) return TestHelper.getComponent(rendered, type);

            var instance = rendered._instance || rendered.stateNode;
            if (instance) {
                instance.waitForAllDispatch = TestHelper.__waitForAllDispatch(instance);
                instance.waitForAllState = TestHelper.__waitForAllState(instance);
            }

            return instance;
        }

        //@depricated

    }, {
        key: 'mockFetch',
        value: function mockFetch(expect) {
            return function () {
                expect(TestHelper.Fetch).toBeDefined();
            };
        }
    }, {
        key: 'isFetchGlobal',
        value: function isFetchGlobal(expect) {
            return function () {
                expect(TestHelper.Fetch).not.toBeNull();
                expect(TestHelper.Fetch).not.toBeUndefined();
            };
        }
    }, {
        key: '__waitForAllDispatch',
        value: function __waitForAllDispatch(component) {
            return function () {
                return new _promise2.default(function (resolve, reject) {
                    component.onDispatchEnd = (0, _throttleDebounce.debounce)(500, function () {
                        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
                            args[_key] = arguments[_key];
                        }

                        resolve(args);
                    });
                });
            };
        }
    }, {
        key: '__waitForAllState',
        value: function __waitForAllState(component) {
            return function () {
                return new _promise2.default(function (resolve, reject) {
                    component.onStateEnd = (0, _throttleDebounce.debounce)(500, function () {
                        for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
                            args[_key2] = arguments[_key2];
                        }

                        resolve(args);
                    });
                });
            };
        }
    }, {
        key: 'mock',
        value: function mock(requests) {
            var resStatus = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 200;

            return function () {
                var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(done) {
                    var i, _requests$i, status, body, url, response, json;

                    return _regenerator2.default.wrap(function _callee$(_context) {
                        while (1) {
                            switch (_context.prev = _context.next) {
                                case 0:
                                    i = 0;

                                case 1:
                                    if (!(i < requests.length)) {
                                        _context.next = 23;
                                        break;
                                    }

                                    _requests$i = requests[i], status = _requests$i.status, body = _requests$i.body, url = _requests$i.url;
                                    _context.prev = 3;
                                    _context.next = 6;
                                    return fetch(url, {
                                        replyWith: {
                                            status: resStatus,
                                            body: (0, _stringify2.default)(body)
                                        }
                                    });

                                case 6:
                                    _context.next = 8;
                                    return fetch(url);

                                case 8:
                                    response = _context.sent;

                                    expect(response.status).toEqual(status);
                                    _context.next = 12;
                                    return response.json();

                                case 12:
                                    json = _context.sent;

                                    expect(json).toEqual(body);
                                    done();
                                    _context.next = 20;
                                    break;

                                case 17:
                                    _context.prev = 17;
                                    _context.t0 = _context['catch'](3);

                                    done(_context.t0);

                                case 20:
                                    i++;
                                    _context.next = 1;
                                    break;

                                case 23:
                                case 'end':
                                    return _context.stop();
                            }
                        }
                    }, _callee, this, [[3, 17]]);
                }));

                return function (_x3) {
                    return _ref.apply(this, arguments);
                };
            }();
        }
    }, {
        key: 'JSON',
        value: function (_JSON) {
            function JSON(_x) {
                return _JSON.apply(this, arguments);
            }

            JSON.toString = function () {
                return _JSON.toString();
            };

            return JSON;
        }(function (rendered) {
            return (0, _stringify2.default)(rendered.toJSON());
        })
    }, {
        key: 'loadScript',
        value: function loadScript(JSDOM, script) {
            var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : DEFAULT_MAP_OPTIONS;

            var _ref2 = new JSDOM(DEFAULT_HTML.replace("%script%", script), options),
                window = _ref2.window;

            return new _promise2.default(function (resolve, reject) {
                window.addEventListener('load', function () {
                    if (window.status.indexOf('error') === -1) return resolve(window);

                    reject(new Error("Can not load google script"));
                });
            });
        }
    }, {
        key: 'loadMap',
        value: function loadMap(jsdom) {
            var key = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
            var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : DEFAULT_MAP_OPTIONS;
            var JSDOM = jsdom.JSDOM;

            var _ref3 = new JSDOM(DEFAULT_HTML.replace("%key%", key), options),
                window = _ref3.window;

            return new _promise2.default(function (resolve, reject) {
                window.addEventListener('load', function () {
                    if (window.status.indexOf('error') === -1) return resolve(window.google);

                    reject(new Error("Can not load google script"));
                });
            });
        }
    }, {
        key: 'Fetch',
        get: function get() {
            return require('fetch-reply-with');
        }
    }]);
    return TestHelper;
}();

exports.default = TestHelper;