import {createStore, applyMiddleware, compose} from 'redux';
import {routerMiddleware} from 'react-router-redux';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import * as Services from '../services';

import rootReducer from '../reducers';

export const history = createHistory();

function configureStore(initialState) {
    return createStore(rootReducer, initialState, compose(
        applyMiddleware(
            thunk.withExtraArgument(Services),
            routerMiddleware(history),
            reduxImmutableStateInvariant(),
        ),
        window.devToolsExtension ? window.devToolsExtension() : f => f));
}

const store = configureStore({});

export default store;