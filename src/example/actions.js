import {EXAMPLE_GET_USER, EXAMPLE_DELETE_USER, EXAMPLE_SAVE_USER} from '../actionTypes';

export default class ExampleActions {
    static getUser(id) {
        return async (dispatch, getState, {UserService}) => {
            const user = UserService.getUser(id);
            dispatch(EXAMPLE_GET_USER({user}));
        }
    }

    static saveUser(user) {
        return async (dispatch, getState, {UserService}) => {
            const output = UserService.saveUser(user);
            dispatch(EXAMPLE_SAVE_USER({user:output}));
        }
    }

    static deleteUser(user) {
        return async (dispatch, getState, {UserService}) => {
            const user = UserService.deleteUser(user);
            dispatch(EXAMPLE_DELETE_USER({user}));
        }
    }
}