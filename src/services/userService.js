
const _dbUser = {};
export default class UserService{
    static getUser(id) {
        return Object.assign({},_dbUser[id],{action:"GET"});
    }

    static saveUser(user) {
        _dbUser[user.id] =  user;
        _dbUser[user.id].action = "SAVE";

        return _dbUser[user.id];
    }

    static deleteUser(id) {
        delete _dbUser[id];
        return _dbUser[id];
    }
}