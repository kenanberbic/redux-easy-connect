import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import {ConnectedRouter} from 'react-router-redux'
import {withRouter} from 'react-router-dom'
import registerServiceWorker from './registerServiceWorker';

import './index.css';
import store, {history} from './store';

import App from './App';
const NonBlockApp = withRouter(App);

ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
           <NonBlockApp/>
        </ConnectedRouter>
    </Provider>, document.getElementById('root'));
registerServiceWorker();
