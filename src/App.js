import React from 'react';
import {Switch, Route} from 'react-router-dom';
import {ReduxComponent} from './lib';
import logo from './logo.svg';
import './App.css';

import Example from './example';
import ExampleActions from './example/actions';
import {UserService} from './services';

export class App extends ReduxComponent {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">redux-easy-connect</h1>
        </header>
        <p className="App-intro">
          Example with tests
        </p>

          <Switch>
              <Route exact path="/" component={(props)=> (
                  <Example {...props}
                           actions={ExampleActions}
                           reducers={{userStore:["user"]}}
                           services={{UserService}}
                  />)} />
          </Switch>
      </div>
    );
  }
}

export default App.CONNECT;
