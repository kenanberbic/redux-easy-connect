/*jshint esversion: 6 */
/*jslint node: true */
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _promise = require("babel-runtime/core-js/promise");

var _promise2 = _interopRequireDefault(_promise);

var _getOwnPropertyNames = require("babel-runtime/core-js/object/get-own-property-names");

var _getOwnPropertyNames2 = _interopRequireDefault(_getOwnPropertyNames);

var _keys = require("babel-runtime/core-js/object/keys");

var _keys2 = _interopRequireDefault(_keys);

var _reactRedux = require("react-redux");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var EXCLUDE_LIST = ["length", "name", "prototype", "arguments", "caller", "apply", "bind", "call", "toString", "constructor"];
function __mapStateToProps(state, props) {
    var store = {};
    if (!props.reducers) return store;

    var reducers = (0, _keys2.default)(props.reducers);
    reducers.map(function (reducer) {
        var fields = props.reducers[reducer];
        if (!fields.length) {
            store[reducer] = state[reducer];
        } else {
            for (var i = 0; i < fields.length; i++) {
                store[fields[i]] = state[reducer][fields[i]];
            }
        }

        return reducer;
    });

    return store;
}

function reduxExtend(Component) {
    Component.prototype.__attach = function (dispatch, baseActions) {
        var actions = {};
        if (!baseActions) return actions;

        var self = this;
        var actionList = (0, _getOwnPropertyNames2.default)(baseActions).concat((0, _getOwnPropertyNames2.default)(baseActions.__proto__));
        actionList.filter(function (x) {
            return EXCLUDE_LIST.indexOf(x) === -1;
        }).map(function (actionName) {
            actions[actionName] = function () {
                var _this = this;

                for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
                    args[_key] = arguments[_key];
                }

                var exec = dispatch(baseActions[actionName].apply(baseActions, args));
                if (!exec) return new _promise2.default(function (resolve) {
                    resolve();
                }).then(function (x) {
                    if (self.onDispatchEnd) self.onDispatchEnd.apply(self, args);

                    return _this;
                });

                exec.then(function (x) {
                    return new _promise2.default(function (resolve) {
                        resolve(x);
                    }).then(function (y) {
                        if (self.onDispatchEnd) self.onDispatchEnd.apply(self, args);

                        return x;
                    });
                });
            };
        });

        return actions;
    };

    Object.defineProperty(Component.prototype, "Actions", {
        get: function Actions() {
            if (!this.actions) this.actions = this.__attach(this.props.dispatch, this.props.actions);

            return this.actions;
        }
    });

    Object.defineProperty(Component.prototype, "Services", {
        get: function Services() {
            return this.props.services;
        }
    });

    return (0, _reactRedux.connect)(__mapStateToProps)(Component);
}

exports.default = reduxExtend;