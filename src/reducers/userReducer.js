import {
    EXAMPLE_GET_USER,
    EXAMPLE_DELETE_USER,
    EXAMPLE_SAVE_USER
} from '../actionTypes';

const initialState = {
    user: null
};

export default function userReducer(state = initialState, action) {
    switch (action.type) {
        case EXAMPLE_GET_USER.type:
            return Object.assign({}, {user:action.user});
        case EXAMPLE_SAVE_USER.type:
            return Object.assign({}, {user:action.user});
        case EXAMPLE_DELETE_USER.type:
            return Object.assign({}, {user:action.user});
        default:
            return state;
    }
}