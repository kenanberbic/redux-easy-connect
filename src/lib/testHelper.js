import {debounce} from 'throttle-debounce';

const DEFAULT_HTML = `
<!DOCTYPE html>
<html>
<head></head>
    <body>
            <script>
                function initMap(e) {
                    window.status = JSON.stringify(e, ["type"]);
                }
                function loadScript(url, callback) {

                    var head = document.getElementsByTagName('head')[0];
                    var script = document.createElement('script');

                    script.type = 'text/javascript';
                    script.onreadystatechange = callback;
                    script.onload = callback;
                    script.onerror = callback;
                    script.src = url;

                    head.appendChild(script);
                }

                loadScript("%script%", initMap);

                </script>
    </body>
    </html>`;

const DEFAULT_MAP_OPTIONS = {
    resources: 'usable',
    runScripts: "dangerously",
    url: "http://localhost:8080",
    referrer: "http://localhost:8080",
    contentType: "text/html",
    userAgent: "Mellblomenator/9000"
};

class TestHelper{
    static get Fetch(){
        return require('fetch-reply-with');
    }

    static getComponent(component, type) {
        if (!component)
            return null;

        if (component._component)
            component = component._component;

        if (component._reactInternalFiber || component._reactInternalInstance)
            component = component._reactInternalFiber || component._reactInternalInstance;

        if (!component)
            return null;

        if (!component._renderedComponent && !component.child)
            return null;

        const rendered = component._renderedComponent || component.child;
        if (rendered &&
            (!rendered._instance || rendered._instance.constructor.name !== type.name) &&
            (!rendered.stateNode || rendered.stateNode.constructor.name !== type.name))
            return TestHelper.getComponent(rendered, type);

        const instance = rendered._instance || rendered.stateNode;
        if (instance) {
            instance.waitForAllDispatch = TestHelper.__waitForAllDispatch(instance);
            instance.waitForAllState = TestHelper.__waitForAllState(instance);
        }

        return instance;
    }

    //@depricated
    static mockFetch(expect) {
        return function () {
            expect(TestHelper.Fetch).toBeDefined();
        }
    }

    static isFetchGlobal() {
        expect(TestHelper.Fetch).not.toBeNull();
        expect(TestHelper.Fetch).not.toBeUndefined();
    }

    static __waitForAllDispatch(component) {
        return ()=> {
            return new Promise((resolve, reject) => {
                component.onDispatchEnd = debounce(500, function (...args) {
                    resolve(args);
                });
            })
        };
    }

    static __waitForAllState(component) {
        return ()=> {
            return new Promise((resolve, reject) => {
                component.onStateEnd = debounce(500, function (...args) {
                    resolve(args);
                });
            })
        };
    }

    static mock(requests, resStatus = 200) {
        return async function (done) {
            for (let i = 0; i < requests.length; i++) {
                const {status, body, url} = requests[i];
                try {
                    await fetch(url, {
                        replyWith: {
                            status: resStatus,
                            body: JSON.stringify(body)
                        }
                    });
                    const response = await fetch(url);
                    expect(response.status).toEqual(status);
                    const json = await response.json();
                    expect(json).toEqual(body);
                    done();
                } catch (ex) {
                    done(ex);
                }
            }
        }
    }

    static JSON(rendered) {
        return JSON.stringify(rendered.toJSON());
    }

    static loadScript(JSDOM, script, options = DEFAULT_MAP_OPTIONS) {
        const {window} = new JSDOM(DEFAULT_HTML.replace("%script%", script), options);

        return new Promise((resolve, reject) => {
            window.addEventListener('load', function () {
                if (window.status.indexOf('error') === -1)
                    return resolve(window);

                reject(new Error("Can not load google script"));
            });
        });
    }

    static loadMap(jsdom, key="", options = DEFAULT_MAP_OPTIONS) {
        const {JSDOM } = jsdom;
        const { window } = new JSDOM(DEFAULT_HTML.replace("%key%", key), options);

        return new Promise((resolve, reject) => {
            window.addEventListener('load', function () {
                if (window.status.indexOf('error') === -1)
                    return resolve(window.google);

                reject(new Error("Can not load google script"));
            });
        });
    }
}

export default TestHelper;
