'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.setType = exports.reduxExtend = exports.TestHelper = exports.ReduxComponent = undefined;

var _assign = require('babel-runtime/core-js/object/assign');

var _assign2 = _interopRequireDefault(_assign);

var _reduxComponent = require('./reduxComponent');

var _reduxComponent2 = _interopRequireDefault(_reduxComponent);

var _reduxExtend = require('./reduxExtend');

var _reduxExtend2 = _interopRequireDefault(_reduxExtend);

var _testHelper = require('./testHelper');

var _testHelper2 = _interopRequireDefault(_testHelper);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var setType = function setType(type) {
    var f = function f() {
        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _assign2.default.apply(Object, [{ type: type }].concat(args));
    };f.type = type;return f;
};

exports.ReduxComponent = _reduxComponent2.default;
exports.TestHelper = _testHelper2.default;
exports.reduxExtend = _reduxExtend2.default;
exports.setType = setType;