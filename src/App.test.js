import React from 'react';
import {Provider} from 'react-redux'
import {ConnectedRouter} from 'react-router-redux'
import store, {history} from './store'
import App from './App';

import renderer from 'react-test-renderer';

it('renders app without crashing', () => {
    const rendered = renderer.create(<Provider store={store}>
      <ConnectedRouter history={history}>
        <div>
          <App store={store}/>
        </div>
      </ConnectedRouter>
    </Provider>).toJSON();
    expect(rendered).toBeTruthy();
});