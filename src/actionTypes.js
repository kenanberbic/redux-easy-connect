import {setType} from './lib';

export const EXAMPLE_GET_USER = setType('example/EXAMPLE_GET_USER');
export const EXAMPLE_SAVE_USER = setType("example/EXAMPLE_SAVE_USER");
export const EXAMPLE_DELETE_USER = setType("example/EXAMPLE_DELETE_USER");