import ReduxComponent from './reduxComponent';
import reduxExtend from './reduxExtend';
import TestHelper from './testHelper';

const setType = (type) => {let f = ((...args) => Object.assign({type}, ...args)); f.type = type; return f;};

export {
    ReduxComponent,
    TestHelper,
    reduxExtend,
    setType
};
