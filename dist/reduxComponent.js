'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _keys = require('babel-runtime/core-js/object/keys');

var _keys2 = _interopRequireDefault(_keys);

var _promise = require('babel-runtime/core-js/promise');

var _promise2 = _interopRequireDefault(_promise);

var _getOwnPropertyNames = require('babel-runtime/core-js/object/get-own-property-names');

var _getOwnPropertyNames2 = _interopRequireDefault(_getOwnPropertyNames);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _get2 = require('babel-runtime/helpers/get');

var _get3 = _interopRequireDefault(_get2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _reactRedux = require('react-redux');

var _react = require('react');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*jshint esversion: 6 */
/*jslint node: true */

var EXCLUDE_LIST = ["length", "name", "prototype", "arguments", "caller", "apply", "bind", "call", "toString", "constructor"];

var BaseComponent = function (_Component) {
    (0, _inherits3.default)(BaseComponent, _Component);

    function BaseComponent(props) {
        (0, _classCallCheck3.default)(this, BaseComponent);

        var _this = (0, _possibleConstructorReturn3.default)(this, (BaseComponent.__proto__ || (0, _getPrototypeOf2.default)(BaseComponent)).call(this, props));

        _this.actions = _this.__attach(_this.props.dispatch, props.actions);
        return _this;
    }

    (0, _createClass3.default)(BaseComponent, [{
        key: 'setState',
        value: function setState(state, callback) {
            return (0, _get3.default)(BaseComponent.prototype.__proto__ || (0, _getPrototypeOf2.default)(BaseComponent.prototype), 'setState', this).call(this, state, callback || this.onStateEnd);
        }
    }, {
        key: '__attach',
        value: function __attach(dispatch, baseActions) {
            var actions = {};
            if (!baseActions) return actions;

            var self = this;
            var actionList = (0, _getOwnPropertyNames2.default)(baseActions).concat((0, _getOwnPropertyNames2.default)(baseActions.__proto__));
            actionList.filter(function (x) {
                return EXCLUDE_LIST.indexOf(x) === -1;
            }).map(function (actionName) {
                actions[actionName] = function () {
                    var _this2 = this;

                    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
                        args[_key] = arguments[_key];
                    }

                    var exec = dispatch(baseActions[actionName].apply(baseActions, args));
                    if (!exec) return new _promise2.default(function (resolve) {
                        resolve();
                    }).then(function (x) {
                        if (self.onDispatchEnd) self.onDispatchEnd.apply(self, args);

                        return _this2;
                    });

                    exec.then(function (x) {
                        return new _promise2.default(function (resolve) {
                            resolve(x);
                        }).then(function (y) {
                            if (self.onDispatchEnd) self.onDispatchEnd.apply(self, args);

                            return x;
                        });
                    });
                };
            });

            return actions;
        }
    }, {
        key: 'Actions',
        get: function get() {
            return this.actions;
        }
    }, {
        key: 'Services',
        get: function get() {
            return this.props.services;
        }
    }], [{
        key: '__mapStateToProps',
        value: function __mapStateToProps(state, props) {
            var store = {};
            if (!props.reducers) return store;

            var reducers = (0, _keys2.default)(props.reducers);

            reducers.map(function (reducer) {
                var fields = props.reducers[reducer];
                if (!fields.length) {
                    store[reducer] = state[reducer];
                } else {
                    for (var i = 0; i < fields.length; i++) {
                        store[fields[i]] = state[reducer][fields[i]];
                    }
                }

                return reducer;
            });

            return store;
        }
    }, {
        key: 'CONNECT',
        get: function get() {
            return (0, _reactRedux.connect)(this.__mapStateToProps)(this);
        }
    }]);
    return BaseComponent;
}(_react.Component);

exports.default = BaseComponent;