import React from 'react';
import renderer from 'react-test-renderer';
import {TestHelper} from '../lib';
import store from '../store'

import ExampleActions from '../example/actions';
import CONNECT, {Example} from '../example';

it('renders example empty page', async () => {
    const rendered = renderer.create(
        <CONNECT
            store={store}
            actions={ExampleActions}
            reducers={{userStore:["user"]}}
        />
    );

    const json = rendered.toJSON();
    const messageContainer = json.children[json.children.length - 1];
    expect(messageContainer).toBeInstanceOf(Object);
    expect(messageContainer.children).toBeNull();

    expect(json).toBeTruthy();
});


it('renders example with action button add user', async () => {
    const rendered = renderer.create(
        <CONNECT
            store={store}
            actions={ExampleActions}
            reducers={{userStore:["user"]}}
        />
    );

    const component = TestHelper.getComponent(rendered.getInstance(), Example);
    component.handleAddUser();
    await component.waitForAllDispatch();

    const json = rendered.toJSON();
    const messageContainer = json.children[json.children.length - 1];
    expect(messageContainer).toBeInstanceOf(Object);
    expect(messageContainer.children).not.toBeNull();
    expect(messageContainer.children.length).toEqual(1);
    expect(messageContainer.children[0]).toEqual('{\"id\":1,\"name\":\"Example\",\"email\":\"example@example.com\",\"action\":\"SAVE\"}');
    expect(json).toBeTruthy();
});