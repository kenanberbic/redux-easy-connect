/*jshint esversion: 6 */
/*jslint node: true */
"use strict";

import {connect} from 'react-redux';

const EXCLUDE_LIST = ["length", "name", "prototype","arguments", "caller", "apply", "bind", "call", "toString", "constructor"];
function __mapStateToProps(state, props) {
    let store = {};
    if (!props.reducers)
        return store;

    const reducers = Object.keys(props.reducers);
    reducers.map(reducer => {
        const fields = props.reducers[reducer];
        if (!fields.length) {
            store[reducer] = state[reducer];
        } else {
            for (let i = 0; i < fields.length; i++)
                store[fields[i]] = state[reducer][fields[i]];
        }

        return reducer;
    });

    return store;
}

function reduxExtend(Component) {
    Component.prototype.__attach = function (dispatch, baseActions) {
        let actions = {};
        if (!baseActions)
            return actions;

        const self = this;
        let actionList = Object.getOwnPropertyNames(baseActions).concat(Object.getOwnPropertyNames(baseActions.__proto__));
        actionList.filter(x => EXCLUDE_LIST.indexOf(x) === -1).map(actionName => {
            actions[actionName] = function (...args) {
                const exec = dispatch(baseActions[actionName](...args));
                if (!exec)
                    return new Promise((resolve) => {
                        resolve();
                    }).then(x => {
                        if (self.onDispatchEnd)
                            self.onDispatchEnd(...args);

                        return this;
                    });

                exec.then(x => {
                    return new Promise((resolve) => {
                        resolve(x);
                    }).then(y => {
                        if (self.onDispatchEnd)
                            self.onDispatchEnd(...args);

                        return x;
                    });
                });
            }
        });

        return actions;
    }

    Object.defineProperty(Component.prototype, "Actions", {
        get: function Actions() {
            if(!this.actions)
                this.actions = this.__attach(this.props.dispatch, this.props.actions);

            return this.actions;
        }
    });

    Object.defineProperty(Component.prototype, "Services", {
        get: function Services() {
            return this.props.services;
        }
    });
    
    return connect(__mapStateToProps)(Component);
}

export default reduxExtend;